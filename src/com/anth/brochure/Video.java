package com.anth.brochure;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.VideoView;

public class Video extends Activity{
	private String path = "";
    private VideoView mVideoView;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.video);
        
        mVideoView = (VideoView) findViewById(R.id.videoView);
        /*
        Environment.getExternalStorageDirectory().getAbsolutePath();

        String SD_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
        path = SD_PATH + "/Movies/return.mp4";
        
        mVideoView.setVideoPath(path);
        */
        Intent intent = getIntent();
        int videoRes = intent.getIntExtra("videoRes", 0);
        Uri uri = Uri.parse("android.resource://com.anth.brochure/" + videoRes);
        mVideoView.setVideoURI(uri);
        mVideoView.setMediaController(new MediaController(this));
        mVideoView.requestFocus();
        mVideoView.start();
        
        
    }
}
