package com.anth.brochure.brochure;

import android.app.Dialog;
import android.content.Context;
import android.widget.ProgressBar;

import com.anth.brochure.R;

public class BrochureDialog extends Dialog{
	private Context mContext;
	private ProgressBar mProgressBar;
	
	public BrochureDialog(Context context){
		super(context);
		mContext = context;
		setContentView(R.layout.brochure_dialog);
		
		mProgressBar = (ProgressBar) findViewById(R.id.pbBrochure);
	}

}
