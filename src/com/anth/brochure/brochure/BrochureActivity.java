package com.anth.brochure.brochure;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.anth.brochure.R;
import com.anth.brochure.SideTab;
import com.anth.brochure.Val;

public class BrochureActivity extends Activity{
	private Context mContext;

	private ArrayList<Brochure> mBrList;
	private BrochureLayout mBrLayout;
	
	private boolean firstLoad = true;

	@Override
	public boolean onTouchEvent(MotionEvent event){
		return mBrLayout.onLayoutTouchEvent(event);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//		setContentView(R.layout.brochure);
		mContext = this;

		Intent intent = getIntent();
		int broType = intent.getIntExtra("type", Val.BRO1);
		int layoutPosition = intent.getIntExtra("position", 0);
		
		/** 보여줄 이미지 리스트 분기 **/
		int[] imageList = null;
		switch(broType){
		case Val.BRO1:
			imageList = new int[] { R.drawable.br1_01, R.drawable.br1_02, R.drawable.br1_03, R.drawable.br1_04, R.drawable.br1_05, R.drawable.br1_06 };
			break;
		case Val.BRO2:
			imageList = new int[] { R.drawable.br2_01, R.drawable.br2_02 };
			break;
		case Val.SPEC1:
			imageList = new int[] { R.drawable.spec1_01 };
			break;
		case Val.SPEC2:
			imageList = new int[] { R.drawable.spec2_01 };
			break;
		}

		/** 이미지 리스트로 브로셔 리스트 생성 **/
		mBrList = new ArrayList<Brochure>();
		for(int curImage : imageList){
			Brochure curBrochure = new Brochure(curImage);
			mBrList.add(curBrochure);
		}

		/** 각 브로셔페이지 어댑터 이용하여 생성 **/
		mBrLayout = new BrochureLayout(this, R.id.ivBrochure);
		mBrLayout.setPaddingWidth(5);
		mBrLayout.setAdapter(new ArrayAdapter<Brochure>(mContext, android.R.layout.simple_list_item_1, mBrList){
			@Override
			public View getView(int position, View convertView, ViewGroup parent){
				if(convertView == null){
					LayoutInflater inflater = getLayoutInflater();
					convertView = inflater.inflate(R.layout.brochure, parent, false);
				}

				ImageView ivLayout = (ImageView) convertView.findViewById(R.id.ivBrochure);
				int imageRes = mBrList.get(position).getImage();
				
				/** 수정사항**/
//				ivLayout.setImageResource(imageRes);
				new BrochureAsyncTask(mContext, ivLayout, imageRes, firstLoad).execute(100);
				firstLoad = false;

				return convertView;
			}
		}, layoutPosition);
		
		/** 화면에 보여줄 뷰 생성 **/
		FrameLayout layout = new FrameLayout(mContext);
		LinearLayout llContent = new LinearLayout(mContext);
		llContent.setOrientation(LinearLayout.HORIZONTAL);

		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT);

		layoutParams.setMargins(10, 10, 10, 10);
		layoutParams.weight = 1.0f;
		llContent.addView(mBrLayout, layoutParams);

		LinearLayout llSideTab = new SideTab(mContext);
		llSideTab.setGravity(Gravity.RIGHT);
		
		layout.addView(llContent);
		layout.addView(llSideTab);
		setContentView(layout);
	}
}
