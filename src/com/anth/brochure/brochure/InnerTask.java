package com.anth.brochure.brochure;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

public class InnerTask extends AsyncTask<Integer, Integer, Integer>{
	private Context mContext;
	private ProgressDialog mProgressDialog;
//	private ImageView ivLayout;
//	private int imageRes;
	
	public InnerTask(Context context){
		mContext = context;
	}
	protected void onPreExecute(){
		mProgressDialog = new ProgressDialog(mContext);
		//		mProgressDialog.setProgressStyle(ProgressDialog.);
		mProgressDialog.setMessage("Loading...");
		mProgressDialog.setCancelable(false);
		mProgressDialog.show();
	}

	@Override
	protected Integer doInBackground(Integer... params) {
		return params[0];
	}

	protected void onPostExecute(Integer result){
		mProgressDialog.dismiss();
	}

	protected void onCancelled(){
		mProgressDialog.dismiss();		
	}
}
