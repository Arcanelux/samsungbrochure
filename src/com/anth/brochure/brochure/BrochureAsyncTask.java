package com.anth.brochure.brochure;

import com.anth.brochure.R;

import android.R.color;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.view.Window;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class BrochureAsyncTask extends AsyncTask<Integer, Integer, Integer>{
	private Context mContext;
	private ProgressDialog mProgressDialog;
	private ProgressBar mProgressBar;
	private Dialog mDialog;
	private BrochureDialog mBrochureDialog;
	private ImageView ivLayout;
	private int imageRes;

	private boolean firstLoad;

	public BrochureAsyncTask(Context context, ImageView ivLayout, int imageRes, boolean firstLoad){
		mContext = context;
		this.ivLayout = ivLayout;
		this.imageRes = imageRes;
		this.firstLoad = firstLoad;
	}
	protected void onPreExecute(){
		if(firstLoad){
			mProgressDialog = new ProgressDialog(mContext);
			mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			mProgressDialog.setMessage("Loading...");
			mProgressDialog.setCancelable(false);
			mProgressDialog.show();
		}

		//		mProgressBar = new ProgressBar(mContext, null, android.R.attr.progressBarStyleSmall);

		//		mDialog = new Dialog(mContext, R.style.NewDialog);
		//		mDialog.addContentView(mProgressBar, null);
		//		mDialog.show();

		//		mBrochureDialog = new BrochureDialog(context)
		//		mBrochureDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		//		mBrochureDialog.getWindow().setBackgroundDrawable(new ColorDrawable(color.transparent));
		//		mBrochureDialog.show();
	}

	@Override
	protected Integer doInBackground(Integer... params) {
		try{
			Thread.sleep(700);
		} catch(Exception e){
			//			Log.d(TAG, "thread error");
		}
		return params[0];
	}

	protected void onPostExecute(Integer result){
		ivLayout.setImageResource(imageRes);
		//		try{
		//			Thread.sleep(500);
		//		} catch(Exception e){
		//			Log.d(TAG, "thread error");
		//		}
		//		mDialog.dismiss();
		if(firstLoad){ mProgressDialog.dismiss(); }
		//		mBrochureDialog.dismiss();
		super.onPostExecute(result);
	}

	protected void onCancelled(){
		if(firstLoad){ mProgressDialog.dismiss(); }
		//		mDialog.dismiss();
		//		mBrochureDialog.dismiss();
		super.onCancelled();
	}
}

