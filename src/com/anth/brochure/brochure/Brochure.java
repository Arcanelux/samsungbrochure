package com.anth.brochure.brochure;

import java.util.ArrayList;

public class Brochure {
	private int image;
	
	public Brochure(int image){
		this.image = image;
	}

	public int getImage() {
		return image;
	}

	public void setImage(int image) {
		this.image = image;
	}
}
