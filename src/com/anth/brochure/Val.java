package com.anth.brochure;

public class Val {
	public static final int BRO1 = 100;
	public static final int BRO2 = 101;
	public static final int SPEC1 = 102;
	public static final int SPEC2 = 103;
	
	public static final int NONE = 119;
	public static final int HOME = 110;
	public static final int HEMS = 111;
	public static final int BROCHURE = 112;
	public static final int VIDEO = 113;
	public static final int SPECSHEET = 114;
	
	/** SideTab **/
	public static int CURPOS = NONE;
	public static boolean TAB_OPEN = false;
}
