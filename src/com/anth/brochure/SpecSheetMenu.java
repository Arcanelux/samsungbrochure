package com.anth.brochure;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.anth.brochure.brochure.BrochureActivity;

public class SpecSheetMenu extends Activity implements OnClickListener{
private Button btn1, btn2;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		LayoutInflater inflater =getLayoutInflater();
		FrameLayout layout = new FrameLayout(this);
		View layoutContent = inflater.inflate(R.layout.specsheet_menu, null);
		LinearLayout llSideTab = new SideTab(this);
		llSideTab.setGravity(Gravity.RIGHT);
		
		layout.addView(layoutContent);
		layout.addView(llSideTab);
		setContentView(layout);
		
		btn1 = (Button) findViewById(R.id.btnSpecMenu1);
		btn2 = (Button) findViewById(R.id.btnSpecMenu2);
		
		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
			case R.id.btnSpecMenu1:
				Intent intent1 = new Intent(SpecSheetMenu.this, BrochureActivity.class);
				intent1.putExtra("type", Val.SPEC1);
				startActivity(intent1);
				break;
			case R.id.btnSpecMenu2:
				Intent intent2 = new Intent(SpecSheetMenu.this, BrochureActivity.class);
				intent2.putExtra("type", Val.SPEC2);
				startActivity(intent2);
				break;
		}
		
	}
}
