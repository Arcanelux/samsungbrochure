package com.anth.brochure;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.anth.brochure.brochure.BrochureMenu;


public class Main extends Activity implements OnClickListener{
	private final String packageName = "com.samsung.smartgrid.hems.ihd.tabDP";
	Intent intentHEMS, intentVideo, intentBrochure, intentSpecSheet;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

		setContentView(R.layout.main);

		intentHEMS = this.getPackageManager().getLaunchIntentForPackage(packageName);
		intentVideo = new Intent(this, VideoMenu.class);
		intentBrochure = new Intent(this, BrochureMenu.class);
		intentSpecSheet = new Intent(this, SpecSheetMenu.class);
		//		intentSpecSheet.putExtra("from", 7);

		ImageView ivHEMS = (ImageView) findViewById(R.id.ivHEMS);
		ivHEMS.setOnClickListener(this);

		ImageView ivVideo = (ImageView) findViewById(R.id.ivVideo);
		ivVideo.setOnClickListener(this);

		ImageView ivBr = (ImageView) findViewById(R.id.ivBr);
		ivBr.setOnClickListener(this);

		ImageView ivSs = (ImageView) findViewById(R.id.ivSs);
		ivSs.setOnClickListener(this);


	}
	@Override
	public void onBackPressed(){
		moveTaskToBack(true);
		android.os.Process.killProcess(android.os.Process.myPid());
	}

	/**
	 * 스플래시(로딩화면)표시하는 것과 초기화를 동시에 진행시키기위해 쓰레드처리
	 */
	private void initialize(){
		InitializationRunnable init = new InitializationRunnable();
		new Thread(init).start();
	}

	/**
	 * 초기화 작업 처리
	 */
	class InitializationRunnable implements Runnable{
		public void run(){
			//여기서부터 초기화 작업 처리
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.ivHEMS:
			try{
				startActivity(intentHEMS);
			}catch(Exception e){
				Toast.makeText(getApplicationContext(), "Cannot found HEMS!", Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.ivBr:
			startActivity(intentBrochure);
			break;
		case R.id.ivVideo:
			startActivity(intentVideo);
			break;
		case R.id.ivSs:
			startActivity(intentSpecSheet);
			break;

		}
	}
}