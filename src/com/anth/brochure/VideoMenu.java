package com.anth.brochure;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;

public class VideoMenu extends Activity implements OnClickListener{
	private int[] btnList = { R.id.btnVideo1, R.id.btnVideo2, R.id.btnVideo3, R.id.btnVideo4, R.id.btnVideo5 };
//	private int[] videoList = { R.raw.intersolar, R.raw.bigbattery, R.raw.batterie_def720p,R.raw.samsung4,R.raw.smartsolution5 };
	private int[] videoList;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		LayoutInflater inflater =getLayoutInflater();
		FrameLayout layout = new FrameLayout(this);
		View layoutContent = inflater.inflate(R.layout.video_menu, null);
		LinearLayout llSideTab = new SideTab(this);
		llSideTab.setGravity(Gravity.RIGHT);
		
		layout.addView(layoutContent);
		layout.addView(llSideTab);
		setContentView(layout);
		
		for(int curBtn : btnList){
			ImageButton curVideoBtn = (ImageButton) findViewById(curBtn);
			curVideoBtn.setOnClickListener(this);
		}
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnVideo1:
			Intent intent1= new Intent(VideoMenu.this, Video.class);
			intent1.putExtra("video", videoList[0]);
			break;
		case R.id.btnVideo2:
			Intent intent2= new Intent(VideoMenu.this, Video.class);
			intent2.putExtra("video", videoList[1]);
			break;
		case R.id.btnVideo3:
			Intent intent3= new Intent(VideoMenu.this, Video.class);
			intent3.putExtra("video", videoList[2]);
			break;
		case R.id.btnVideo4:
			Intent intent4= new Intent(VideoMenu.this, Video.class);
			intent4.putExtra("video", videoList[3]);
			break;
		case R.id.btnVideo5:
			Intent intent5= new Intent(VideoMenu.this, Video.class);
			intent5.putExtra("video", videoList[4]);
			break;
		}
	}
}
