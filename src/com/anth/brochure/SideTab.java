package com.anth.brochure;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.anth.brochure.brochure.BrochureMenu;

public class SideTab extends LinearLayout implements OnClickListener{
	private Context mContext;
	private int tabWidth;
	private int duration = 250;
	private final String packageName = "com.samsung.smartgrid.hems.ihd.tabDP";
	
	public Button btnHome, btnHEMS, btnVideo, btnBrochure, btnSpecsheet;
	public LinearLayout llTab, llTabContent;
	FrameLayout tabToggle;
	
	public SideTab(Context context) {
		super(context);
		mContext = context;
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		llTab = (LinearLayout) inflater.inflate(R.layout.side_tab, null);
		llTabContent = (LinearLayout) llTab.findViewById(R.id.sideTabBtnLayout);
		
		if(!Val.TAB_OPEN){ hideTab(0); }
		
		tabToggle = (FrameLayout) llTab.findViewById(R.id.sideTabOpenLayout);
		tabToggle.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				toggleTab();
			}
		});
		
		btnHome = (Button) llTab.findViewById(R.id.btnHome);
		btnHEMS = (Button) llTab.findViewById(R.id.btnHems);
		btnVideo = (Button) llTab.findViewById(R.id.btnVideo);
		btnBrochure= (Button) llTab.findViewById(R.id.btnBrochure);
		btnSpecsheet = (Button) llTab.findViewById(R.id.btnSpecsheet);
		
		btnHome.setOnClickListener(this);
		btnHEMS.setOnClickListener(this);
		btnVideo.setOnClickListener(this);
		btnBrochure.setOnClickListener(this);
		btnSpecsheet.setOnClickListener(this);
		
		addView(llTab);
//		WindowManager.LayoutParams params = (android.view.WindowManager.LayoutParams) llTab.getLayoutParams();
//		int x = params.x;
//		int y = params.y;
		setButtonClicked();
	}
	
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnHome:
			Val.CURPOS = Val.NONE;
			Val.TAB_OPEN = false;
			Intent intentHome = new Intent((Activity) mContext, Main.class);
			mContext.startActivity(intentHome);
			((Activity) mContext).finish();
			break;
		case R.id.btnHems:
			Val.CURPOS = Val.NONE;
			Intent intentHEMS = mContext.getPackageManager().getLaunchIntentForPackage(packageName);
			try{
				mContext.startActivity(intentHEMS);
				((Activity) mContext).finish();
			}catch(Exception e){
				Toast.makeText(mContext.getApplicationContext(), "Cannot found HEMS!", Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.btnVideo:
			Val.CURPOS = Val.VIDEO;
			Intent intentVideo = new Intent((Activity) mContext, VideoMenu.class);
			mContext.startActivity(intentVideo);
			((Activity) mContext).finish();
			break;
		case R.id.btnBrochure:
			Val.CURPOS = Val.BROCHURE;
			Intent intentBro = new Intent((Activity) mContext, BrochureMenu.class);
			mContext.startActivity(intentBro);
			((Activity) mContext).finish();
			break;
		case R.id.btnSpecsheet:
			Val.CURPOS = Val.SPECSHEET;
			Intent intentSpec = new Intent((Activity) mContext, SpecSheetMenu.class);
			mContext.startActivity(intentSpec);
			((Activity) mContext).finish();
			break;
		}
	}
	
	public void toggleTab(){
		if(Val.TAB_OPEN)	hideTab(duration);
		else showTab(duration);
	}
	
	public void showTab(int time){
		if(tabWidth==0) tabWidth = llTabContent.getWidth();
		AnimationSet showTab = new AnimationSet(false);
//		Animation translate = new TranslateAnimation(llTabContent.getWidth(), 0, 0, 0);
		Animation translate = new TranslateAnimation(tabWidth, 0, 0, 0);
		translate.setDuration(time);
		showTab.addAnimation(translate);
		showTab.setFillAfter(true);
		showTab.setAnimationListener(new AnimationListener(){
			@Override
			public void onAnimationEnd(Animation animation) {}
			@Override
			public void onAnimationRepeat(Animation animation) {}
			@Override
			public void onAnimationStart(Animation animation) {
				llTabContent.setVisibility(View.VISIBLE);
			}
		});
		llTab.startAnimation(showTab);

		Val.TAB_OPEN = true;
	}
	
	public void hideTab(int time){
		if(tabWidth==0) tabWidth = llTabContent.getWidth();
		AnimationSet hideTab = new AnimationSet(false);
//		Animation translate = new TranslateAnimation(0, llTabContent.getWidth(), 0, 0);
		Animation translate = new TranslateAnimation(0, tabWidth, 0, 0);
		translate.setDuration(time);
		hideTab.addAnimation(translate);
//		hideTab.setFillAfter(true);
		hideTab.setFillAfter(false);
		hideTab.setAnimationListener(new AnimationListener(){
			@Override
			public void onAnimationEnd(Animation animation) {
				llTabContent.setVisibility(View.GONE);
			}
			@Override
			public void onAnimationRepeat(Animation animation) {}
			@Override
			public void onAnimationStart(Animation animation) {}
		});
		llTab.startAnimation(hideTab);
		
		Val.TAB_OPEN = false;		
	}
	
	private void setButtonClicked(){
		switch(Val.CURPOS){
		case Val.NONE:
//			btnHome.setBackgroundResource(R.drawable.rbar_open_bt_1);
			btnHEMS.setBackgroundResource(R.drawable.rbar_open_bt_2);
			btnVideo.setBackgroundResource(R.drawable.rbar_open_bt_3);
			btnBrochure.setBackgroundResource(R.drawable.rbar_open_bt_4);
			btnSpecsheet.setBackgroundResource(R.drawable.rbar_open_bt_5);
			break;
		case Val.HOME:
//			btnHEMS.setBackgroundResource(R.drawable.rbar_open_bt_2);
//			btnVideo.setBackgroundResource(R.drawable.rbar_open_bt_3);
//			btnBrochure.setBackgroundResource(R.drawable.rbar_open_bt_4);
//			btnSpecsheet.setBackgroundResource(R.drawable.rbar_open_bt_5);
			break;
		case Val.HEMS:
//			btnHEMS.setBackgroundResource(R.drawable.rbar_open_bt_2_d);
//			btnVideo.setBackgroundResource(R.drawable.rbar_open_bt_3);
//			btnBrochure.setBackgroundResource(R.drawable.rbar_open_bt_4);
//			btnSpecsheet.setBackgroundResource(R.drawable.rbar_open_bt_5);
			break;
		case Val.VIDEO:
			btnHEMS.setBackgroundResource(R.drawable.rbar_open_bt_2);
			btnVideo.setBackgroundResource(R.drawable.rbar_open_bt_3_d);
			btnBrochure.setBackgroundResource(R.drawable.rbar_open_bt_4);
			btnSpecsheet.setBackgroundResource(R.drawable.rbar_open_bt_5);
			break;
		case Val.BROCHURE:
			btnHEMS.setBackgroundResource(R.drawable.rbar_open_bt_2);
			btnVideo.setBackgroundResource(R.drawable.rbar_open_bt_3);
			btnBrochure.setBackgroundResource(R.drawable.rbar_open_bt_4_d);
			btnSpecsheet.setBackgroundResource(R.drawable.rbar_open_bt_5);
			break;
		case Val.SPECSHEET:
			btnHEMS.setBackgroundResource(R.drawable.rbar_open_bt_2);
			btnVideo.setBackgroundResource(R.drawable.rbar_open_bt_3);
			btnBrochure.setBackgroundResource(R.drawable.rbar_open_bt_4);
			btnSpecsheet.setBackgroundResource(R.drawable.rbar_open_bt_5_d);
			break;
		}
	}
}
