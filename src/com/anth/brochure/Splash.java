package com.anth.brochure;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;

public class Splash extends Activity{
	private AnimationSet opening, logo;
	private View openingImage, logoImage;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		
		setContentView(R.layout.splash);
		
		opening = new AnimationSet(false);
		logo = new AnimationSet(false);
		
		openingImage = findViewById(R.id.openingImage);
		logoImage = findViewById(R.id.logoImage);
		
		/**
		 * opening관련
		 */
		Animation scale = new ScaleAnimation(
							0.0f, 15f,
							0.0f, 15f,
							Animation.RELATIVE_TO_SELF, 0.5f,
							Animation.RELATIVE_TO_SELF, 0.5f);
		scale.setDuration(2000);
		scale.setFillAfter(true);
		
		opening.addAnimation(scale);
		opening.setFillAfter(true);
		
		
		/**
		 * logo관련
		 */
		Animation logoScale = new ScaleAnimation(
								0.0f, 1.2f,
								0.0f, 1.2f,
								Animation.RELATIVE_TO_SELF, 0.5f,
								Animation.RELATIVE_TO_SELF, 0.5f);
		logoScale.setDuration(1000);
		logoScale.setInterpolator(AnimationUtils.loadInterpolator(getApplicationContext(), android.R.anim.overshoot_interpolator));
		logoScale.setFillAfter(true);
		logoScale.setStartTime(500);
		
		logo.addAnimation(logoScale);
		logo.setFillAfter(true);
		
		
		openingImage.startAnimation(opening);
		logoImage.startAnimation(logo);
	
		
		
		initialize();
	}
	
	private void initialize(){
		Handler handler = new Handler(){
			@Override
			public void handleMessage(Message msg){
				startActivity(new Intent(Splash.this, Main.class));
				finish();
			}
		};
		handler.sendEmptyMessageDelayed(0,2500);	//로딩화면 시간설정
	}

}
